import './App.css';
import React from 'react';
import Login from './Login';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import VideoItem from './RenderVideo';
function App() {
  return (
   <BrowserRouter>
    <Routes>
      <Route path='/' element={<Login/>} />
      <Route path='/video' element={<VideoItem/>} />
    </Routes>
   </BrowserRouter>
  );
}

export default App;
