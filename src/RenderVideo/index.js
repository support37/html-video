import React, { useEffect, useState } from 'react'
import BaseAxios from '../setupAxios'
import './index.css'

function VideoItem() {
  const [videos, setVideos] = useState([]);
  const [currentVideo, setCurrentVideo] = useState(0);
  const [page, setPage] = useState(0)

  useEffect(() => {
    BaseAxios(
      {
        url: '/videos',
        method: 'GET',
        params: {
          page: page + 1
        }
      }
    ).then((data)=>{
      setVideos([...videos, ...data.data.data.data])
    }).catch((error)=>{
    })
  }, [])

  useEffect(() => {
    if(currentVideo === videos.length - 1){
      setPage(page + 1)
            BaseAxios(
        {
          url: '/videos',
          method: 'GET',
          params: {
            page: page + 1
          }
        }
      ).then((data)=>{
        setVideos([...videos, ...data.data.data.data])
      }).catch((error)=>{
      })
    }
  }, [currentVideo])


  function handleDownClick() {
    if (currentVideo < videos.length - 1) {
      setCurrentVideo(currentVideo + 1);
    }
  }

  function handleUpClick() {
    if (currentVideo > 0) {
      setCurrentVideo(currentVideo - 1);
    }
  }
  if (videos.length === 0) {
    return <div>Loading...</div>;
  }

  function handleKeyDown(event) {
    if (event.key === 'ArrowUp') {
      handleUpClick();
    } else if (event.key === 'ArrowDown') {
      handleDownClick();
    }
  }

  function handleKeyUp(event) {
    if (event.key === 'ArrowUp' || event.key === 'ArrowDown') {
      event.preventDefault();
    }
  }
 
  // useEffect(() => {
  //   document.addEventListener('keydown', handleKeyDown);
  //   document.addEventListener('keyup', handleKeyUp);
  //   return () => {
  //     document.removeEventListener('keydown', handleKeyDown);
  //     document.removeEventListener('keyup', handleKeyUp);
  //   };
  // }, [handleKeyDown, handleKeyUp]);

  return (
    <div>
      <div className="video-container" onKeyDown={handleKeyDown} tabIndex="0" onKeyUp={handleKeyUp}>
        <div className="video">
          <video
            src={videos[currentVideo]?.video}
            controls
            autoPlay
            loop
          ></video>
        </div>
        <div className="video-controls">
          <button className='buttonUp' onClick={handleUpClick}>🔼</button>
          <button className='buttonDown' onClick={handleDownClick}>🔽</button>
        </div>
      </div>
    </div>

  )
}

export default VideoItem